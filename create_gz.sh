#!/bin/bash

tar -cvf - Git/ \
    Bin/ \
    Linux/ \
    Miscellaneous/ \
    Mobile/ \
    Network/ \
    Web/ \
    Windows/ \
    Wordlists/ \
    .gitmodules \
    update.sh \
    README.md \
    build_golang_tools.sh \
    create_gz.sh \
    add_submodules.sh | gzip -9 - > tools_$(date "+%d-%b-%Y_%H%M").tar.gz
