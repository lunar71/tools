#!/bin/bash
function build() {
    export PATH=$PATH:/usr/local/go/bin:$HOME/go:$HOME/go/bin
    [[ ! $(command -v go) ]] && printf "Golang not found, exiting...\n" && exit 1

    BASEDIR=$(pwd)

    MAIN=$(find . -name "main.go")
    for i in $MAIN; do
        DIR=$(dirname $i)
        NAME=$(basename $DIR)
        cd $DIR && GO111MODULE=auto go get -d && GO111MODULE=auto go build -ldflags "-s -w"; cd $BASEDIR
        cd $DIR && GO111MODULE=auto GOOS=windows GOARCH=amd64 go build -ldflags "-s -w"; cd $BASEDIR

        mv $DIR/$NAME ./Bin/Linux
        mv $DIR/$NAME.exe ./Bin/Windows
    done
}
build
