# Tools list

	Bin
	|-- Bin/hashcat-6.1.1.7z
	|-- Bin/Linux
	|   |-- Bin/Linux/amass
	|   |-- Bin/Linux/aquatone
	|   |-- Bin/Linux/aquatone_linux_amd64_1.7.0.zip
	|   |-- Bin/Linux/assetfinder
	|   |-- Bin/Linux/bench
	|   |-- Bin/Linux/bettercap
	|   |-- Bin/Linux/BloodHound-linux-x64.zip
	|   |-- Bin/Linux/chisel
	|   |-- Bin/Linux/cloudlist
	|   |-- Bin/Linux/ffuf
	|   |-- Bin/Linux/gau
	|   |-- Bin/Linux/gobuster
	|   |-- Bin/Linux/go-donut
	|   |-- Bin/Linux/hakrawler
	|   |-- Bin/Linux/httprobe
	|   |-- Bin/Linux/kerbrute
	|   |-- Bin/Linux/naabu
	|   |-- Bin/Linux/nuclei
	|   |-- Bin/Linux/pspy
	|   |-- Bin/Linux/revsocks
	|   |-- Bin/Linux/subfinder
	|   |-- Bin/Linux/subjs
	|   |-- Bin/Linux/urlgrab
	|   |-- Bin/Linux/waybackurls
	|   `-- Bin/Linux/ysoserial-master-SNAPSHOT.jar
	|-- Bin/Mobile
	|   |-- Bin/Mobile/apktool
	|   |-- Bin/Mobile/apktool_2.4.1.jar
	|   |-- Bin/Mobile/dex-tools-2.0.zip
	|   |-- Bin/Mobile/jd-gui-1.6.6-min.jar
	|   |-- Bin/Mobile/sign-1.0.jar
	|   `-- Bin/Mobile/signapk-1.0.jar
	`-- Bin/Windows
	    |-- Bin/Windows/amass.exe
	    |-- Bin/Windows/aquatone.exe
	    |-- Bin/Windows/aquatone_windows_amd64_1.7.0.zip
	    |-- Bin/Windows/assetfinder.exe
	    |-- Bin/Windows/bench.exe
	    |-- Bin/Windows/BloodHound-win32-x64.zip
	    |-- Bin/Windows/chisel.exe
	    |-- Bin/Windows/cloudlist.exe
	    |-- Bin/Windows/ffuf.exe
	    |-- Bin/Windows/gau.exe
	    |-- Bin/Windows/gobuster.exe
	    |-- Bin/Windows/go-donut.exe
	    |-- Bin/Windows/hakrawler.exe
	    |-- Bin/Windows/httprobe.exe
	    |-- Bin/Windows/kerbrute.exe
	    |-- Bin/Windows/mimikatz_trunk.7z
	    |-- Bin/Windows/naabu.exe
	    |-- Bin/Windows/nuclei.exe
	    |-- Bin/Windows/revsocks.exe
	    |-- Bin/Windows/subfinder.exe
	    |-- Bin/Windows/subjs.exe
	    |-- Bin/Windows/urlgrab.exe
	    |-- Bin/Windows/waybackurls.exe
	    `-- Bin/Windows/ysoserial-1.34.zip
	Wordlists
	|-- awesome-default-passwords
	|-- CRLF-Injection-Payloads
	|-- DefaultCreds-cheat-sheet
	|-- fuzzdb
	|-- Gf-Patterns
	|-- gf-secrets
	|-- ics-default-passwords
	|-- IntruderPayloads
	|-- jwt-secrets
	|-- jwt_secrets
	|-- Linux-default-files-images-location
	|-- Markdown-XSS-Payloads
	|-- Open-Redirect-Payloads
	|-- PayloadsAllTheThings
	|-- Probable-Wordlists
	|-- Pwdb-Public
	|-- resolvers
	|-- RobotsDisallowed
	|-- SecLists
	|-- subdomain-bruteforce-list
	|-- Virtual-host-wordlist
	`-- wordlist
	Web
	|-- Amass
	|-- aquatone
	|-- Arjun
	|-- assetfinder
	|-- Bug-Bounty-Toolz
	|-- c-jwt-cracker
	|-- cloudlist
	|-- CMSmap
	|-- ct-exposer
	|-- dirsearch
	|-- ffuf
	|-- gau
	|-- gobuster
	|-- hakrawler
	|-- htshells
	|-- httprobe
	|-- httpx
	|-- IIS-ShortName-Scanner
	|-- InputScanner
	|-- JoomlaScan
	|-- JS-Scan
	|-- LinkFinder
	|-- marshalsec
	|-- naabu
	|-- nuclei
	|-- nuclei-templates
	|-- parameth
	|-- Parth
	|-- S3Scanner
	|-- SecretFinder
	|-- server-status_PWN
	|-- SharpBuster
	|-- sslscan
	|-- subfinder
	|-- subjs
	|-- urlgrab
	|-- waybackurls
	|-- WhatWeb
	|-- wpscan
	`-- wstg
	Network
	|-- Exploitation
	|   |-- bettercap
	|   |-- CrackMapExec
	|   |-- eaphammer
	|   |-- impacket
	|   |-- mitm6
	|   |-- net-creds
	|   |-- PRET
	|   |-- Responder
	|   |-- routersploit
	|   |-- SMBGhost
	|   `-- wifiphisher
	|-- Pivoting
	|   |-- chisel
	|   |-- evil-winrm
	|   |-- hershell
	|   |-- pwncat
	|   |-- revsocks
	|   |-- rpivot
	|   `-- vlan-hopping---frogger
	`-- Scanning
	    |-- BloodHound
	    |-- BloodHound.py
	    |-- CVE-2020-0796
	    |-- eavesarp
	    |-- masscan
	    |-- nmap-vulners
	    `-- smbmap
	Windows
	|-- AD
	|   |-- AADInternals
	|   |-- ADModule
	|   |-- Grouper2
	|   |-- LAPSToolkit
	|   |-- nishang
	|   |-- PowerShell-AD-Recon
	|   |-- PowerSploit
	|   |-- PowerUpSQL
	|   |-- Recon-AD
	|   |-- SharpSploit
	|   `-- UhOh365
	|-- Bypasses
	|   |-- HiddenPowerShellDll
	|   |-- Invisi-Shell
	|   |-- PowerShdll
	|   `-- PSByPassCLM
	|-- Enumerators
	|   |-- DecryptRDCManager
	|   |-- JAWS
	|   |-- Powerless
	|   |-- privilege-escalation-awesome-scripts-suite
	|   |-- Seatbelt
	|   |-- SessionGopher
	|   `-- Watson
	|-- Kerberos
	|   |-- kerberoast
	|   |-- kerbrute
	|   |-- RiskySPN
	|   |-- Rubeus
	|   |-- ticket_converter
	|   `-- tickey
	|-- Loaders
	|   |-- DLLsForHackers
	|   |-- DotNetInject
	|   |-- GhostBuild
	|   |-- MaliciousDLLGen
	|   |-- PowerLine
	|   |-- SharpShooter
	|   |-- TikiTorch
	|   `-- unicorn
	|-- Miscellaneous
	|   |-- Amsi-Bypass-Powershell
	|   |-- AmsiScanBufferBypass
	|   |-- awsmBloodhoundCustomQueries
	|   |-- BloodHoundQueries
	|   |-- chimera
	|   |-- CobaltStrikeScan
	|   |-- ConfuserEx
	|   |-- Create-EXEFromPS1
	|   |-- DefenderCheck
	|   |-- donut
	|   |-- go-donut
	|   |-- Invoke-PSImage
	|   |-- Lockless
	|   |-- Microsoft-Activation-Scripts
	|   |-- nccfsas
	|   |-- OffensivePowerShell
	|   |-- powerglot
	|   |-- PowerShellArsenal
	|   |-- Powershellery
	|   |-- PSBits
	|   |-- SharpCollection
	|   |-- SharPersist
	|   `-- SharpXOR
	|-- Pivoting
	|   |-- CSharpWinRM
	|   |-- Inveigh
	|   |-- InveighZero
	|   |-- Invoke-SocksProxy
	|   |-- Invoke-TheHash
	|   |-- powercat
	|   |-- Salsa-tools
	|   |-- SharpRDP
	|   `-- WinPwn
	`-- Privesc
	    |-- DAMP
	    |-- hot-manchego
	    |-- Internal-Monologue
	    |-- juicy-potato
	    |-- KeeThief
	    |-- ntlm_theft
	    |-- Powermad
	    |-- PowerMemory
	    |-- RdpThief
	    |-- RoguePotato
	    |-- RottenPotatoNG
	    |-- RunAsTI
	    |-- SafetyKatz
	    |-- SeBackupPrivilege
	    |-- SharpClipboard
	    |-- SharpDump
	    |-- SharpKatz
	    |-- SharpLocker
	    |-- SharpSecDump
	    |-- SharpUp
	    |-- SpoolSample
	    |-- UACME
	    `-- UAC-SilentClean
	Linux
	|-- Backdoors
	|   `-- 3snake
	|-- Enumerators
	|   |-- LinEnum
	|   |-- linuxprivchecker
	|   |-- linux-smart-enumeration
	|   |-- privilege-escalation-awesome-scripts-suite
	|   |-- SUID3NUM
	|   `-- unix-privesc-check
	`-- Miscellaneous
	    |-- Bashfuscator
	    |-- Decodify
	    |-- pspy
	    |-- pypykatz
	    |-- setuid-wrapper
	    |-- static-arm-bins
	    |-- static-binaries
	    |-- static-binaries-i386
	    `-- static-tools
	Mobile
	|-- Arcane
	`-- frida-ios-dump
	Git
	|-- GitGot
	`-- GitHacker
	Miscellaneous
	|-- Active-Directory-Exploitation-Cheat-Sheet
	|-- awesome-shodan-queries
	|-- CarbonCopy
	|-- CheatSheets
	|-- Ciphey
	|-- duplicut
	|-- hashcat-utils
	|-- hcxdumptool
	|-- hcxtools
	|-- holehe
	|-- homoglyph
	|-- Internal-Pentest-Playbook
	|-- LinkedInt
	|-- notify
	|-- Pentesting
	|-- pe_tree
	`-- rtfm
