#!/bin/bash

git pull --recurse-submodules
tree --charset==ascii --noreport -f Bin > README.md
tree --charset==ascii --noreport -d -L 1 Wordlists >> README.md
tree --charset==ascii --noreport -d -L 1 Web >> README.md
tree --charset==ascii --noreport -d -L 2 Network >> README.md
tree --charset==ascii --noreport -d -L 2 Windows >> README.md
tree --charset==ascii --noreport -d -L 2 Linux >> README.md
tree --charset==ascii --noreport -d -L 1 Mobile >> README.md
tree --charset==ascii --noreport -d -L 1 Git >> README.md
tree --charset==ascii --noreport -d -L 1 Miscellaneous >> README.md
sed -i 's/^/\t/g' README.md
sed -i '1 i\# Tools list\n' README.md
