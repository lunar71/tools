#!/bin/bash

mkdir -p ./{Wordlists,Web,Mobile,Windows/{Miscellaneous,Privesc,Enumerators,Pivoting,Bypasses,AD,Kerberos,Loaders},Linux/{Miscellaneous,Privesc,Enumerators,Pivoting,Backdoors},Network/{Exploitation,Scanning,Pivoting},Git,Miscellaneous,Bin/{Linux,Windows}}

function add_submodules_to_dir() {
    local dir=$1
    shift
    local arr=("$@")
    basedir=$(pwd)
    for i in "${arr[@]}"; do
        p=$(printf "${i}" | awk -F '.com/' '{print $2}')
        cd $dir && git submodule add $i; cd $basedir
    done
}

wordlists=(
    https://github.com/danielmiessler/SecLists
    https://github.com/FlameOfIgnis/Pwdb-Public
    https://github.com/swisskyrepo/PayloadsAllTheThings
    https://github.com/fuzzdb-project/fuzzdb
    https://github.com/1ndianl33t/Gf-Patterns
    https://github.com/dwisiswant0/gf-secrets
    https://github.com/danielmiessler/RobotsDisallowed
    https://github.com/cujanovic/subdomain-bruteforce-list
    https://github.com/cujanovic/Open-Redirect-Payloads
    https://github.com/cujanovic/Markdown-XSS-Payloads
    https://github.com/cujanovic/CRLF-Injection-Payloads
    https://github.com/cujanovic/Virtual-host-wordlist
    https://github.com/cujanovic/resolvers
    https://github.com/cujanovic/Linux-default-files-images-location
    https://github.com/arnaudsoullie/ics-default-passwords
    https://github.com/jeanphorn/wordlist
    https://github.com/nyxxxie/awesome-default-passwords
    https://github.com/1N3/IntruderPayloads
    https://github.com/berzerk0/Probable-Wordlists
    https://github.com/BBhacKing/jwt_secrets
    https://github.com/wallarm/jwt-secrets
    https://github.com/ihebski/DefaultCreds-cheat-sheet
)
add_submodules_to_dir Wordlists "${wordlists[@]}"

web=(
    https://github.com/tomnomnom/waybackurls
    https://github.com/tomnomnom/httprobe
    https://github.com/tomnomnom/assetfinder
    https://github.com/lc/subjs
    https://github.com/lc/gau
    https://github.com/projectdiscovery/cloudlist
    https://github.com/projectdiscovery/naabu
    https://github.com/projectdiscovery/subfinder
    https://github.com/projectdiscovery/httpx
    https://github.com/projectdiscovery/nuclei
    https://github.com/OJ/gobuster
    https://github.com/michenriksen/aquatone
    https://github.com/ffuf/ffuf
    https://github.com/OWASP/Amass
    https://github.com/s0md3v/Arjun
    https://github.com/GerbenJavado/LinkFinder
    https://github.com/m4ll0k/SecretFinder
    https://github.com/brendan-rius/c-jwt-cracker
    https://github.com/OWASP/wstg
    https://github.com/maurosoria/dirsearch
    https://github.com/sa7mon/S3Scanner
    https://github.com/Dionach/CMSmap
    https://github.com/urbanadventurer/WhatWeb
    https://github.com/wpscanteam/wpscan
    https://github.com/zseano/InputScanner
    https://github.com/zseano/JS-Scan
    https://github.com/maK-/parameth
    https://github.com/drego85/JoomlaScan
    https://github.com/hakluke/hakrawler
    https://github.com/mbechler/marshalsec
    https://github.com/rbsec/sslscan
    https://github.com/IAmStoxe/urlgrab
    https://github.com/wireghoul/htshells
    https://github.com/m4ll0k/Bug-Bounty-Toolz
    https://github.com/chris408/ct-exposer
    https://github.com/mazen160/server-status_PWN
    https://github.com/s0md3v/Parth
    https://github.com/irsdl/IIS-ShortName-Scanner
    https://github.com/passthehashbrowns/SharpBuster
    https://github.com/projectdiscovery/nuclei-templates
)
add_submodules_to_dir Web "${web[@]}"

mobile=(
    https://github.com/AloneMonkey/frida-ios-dump
    https://github.com/tokyoneon/Arcane
)
add_submodules_to_dir Mobile "${mobile[@]}"

git=(
    https://github.com/WangYihang/GitHacker
    https://github.com/BishopFox/GitGot
)
add_submodules_to_dir Git "${git[@]}"

# windows
windows_misc=(
    https://github.com/S3cur3Th1sSh1t/Amsi-Bypass-Powershell
    https://github.com/CompassSecurity/BloodHoundQueries
    https://github.com/awsmhacks/awsmBloodhoundCustomQueries
    https://github.com/massgravel/Microsoft-Activation-Scripts
    https://github.com/gtworek/PSBits
    https://github.com/nullbind/Powershellery
    https://github.com/nccgroup/nccfsas
    https://github.com/tokyoneon/chimera
    https://github.com/jfmaes/SharpXOR
    https://github.com/mindcrypt/powerglot
    https://github.com/GhostPack/Lockless
    https://github.com/matterpreter/DefenderCheck
    https://github.com/Flangvik/SharpCollection
    https://github.com/fireeye/SharPersist
    https://github.com/mattifestation/PowerShellArsenal
    https://github.com/nickrod518/Create-EXEFromPS1
    https://github.com/Apr4h/CobaltStrikeScan
    https://github.com/rasta-mouse/AmsiScanBufferBypass
    https://github.com/thecybermafia/OffensivePowerShell
    https://github.com/peewpw/Invoke-PSImage
    https://github.com/TheWover/donut
    https://github.com/Binject/go-donut
    https://github.com/yck1509/ConfuserEx
)
add_submodules_to_dir Windows/Miscellaneous "${windows_misc[@]}"

windows_privesc=(
    https://github.com/jschicht/RunAsTI
    https://github.com/Pickfordmatt/SharpLocker
    https://github.com/antonioCoco/RoguePotato
    https://github.com/ohpe/juicy-potato
    https://github.com/decoder-it/juicy-potato
    https://github.com/breenmachine/RottenPotatoNG
    https://github.com/HarmJ0y/DAMP
    https://github.com/leechristensen/SpoolSample
    https://github.com/Kevin-Robertson/Powermad
    https://github.com/G0ldenGunSec/SharpSecDump
    https://github.com/GhostPack/SharpDump
    https://github.com/GhostPack/SharpUp
    https://github.com/HarmJ0y/SharpClipboard
    https://github.com/EncodeGroup/UAC-SilentClean
    https://github.com/hfiref0x/UACME
    https://github.com/giMini/PowerMemory
    https://github.com/giuliano108/SeBackupPrivilege
    https://github.com/HarmJ0y/KeeThief
    https://github.com/0x09AL/RdpThief
    https://github.com/b4rtik/SharpKatz
    https://github.com/GhostPack/SafetyKatz
    https://github.com/Greenwolf/ntlm_theft
    https://github.com/eladshamir/Internal-Monologue
    https://github.com/FortyNorthSecurity/hot-manchego
)
add_submodules_to_dir Windows/Privesc "${windows_privesc[@]}"

windows_enumerators=(
    https://github.com/M4ximuss/Powerless
    https://github.com/411Hall/JAWS
    https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite
    https://github.com/rasta-mouse/Watson
    https://github.com/mez-0/DecryptRDCManager
    https://github.com/GhostPack/Seatbelt
    https://github.com/Arvanaghi/SessionGopher
)
add_submodules_to_dir Windows/Enumerators "${windows_enumerators[@]}"

windows_pivoting=(
    https://github.com/mez-0/CSharpWinRM
    https://github.com/S3cur3Th1sSh1t/WinPwn
    https://github.com/Hackplayers/Salsa-tools
    https://github.com/0xthirteen/SharpRDP
    https://github.com/p3nt4/Invoke-SocksProxy
    https://github.com/besimorhino/powercat
    https://github.com/Kevin-Robertson/Invoke-TheHash
    https://github.com/Kevin-Robertson/Inveigh
    https://github.com/Kevin-Robertson/InveighZero
)
add_submodules_to_dir Windows/Pivoting "${windows_pivoting[@]}"

windows_bypasses=(
    https://github.com/OmerYa/Invisi-Shell
    https://github.com/b4rtik/HiddenPowerShellDll
    https://github.com/padovah4ck/PSByPassCLM
    https://github.com/p3nt4/PowerShdll
)
add_submodules_to_dir Windows/Bypasses "${windows_bypasses[@]}"

windows_AD=(
    https://github.com/PyroTek3/PowerShell-AD-Recon
    https://github.com/Raikia/UhOh365
    https://github.com/l0ss/Grouper2
    https://github.com/samratashok/ADModule
    https://github.com/Gerenios/AADInternals
    https://github.com/NetSPI/PowerUpSQL
    https://github.com/samratashok/nishang
    https://github.com/cobbr/SharpSploit
    https://github.com/leoloobeek/LAPSToolkit
    https://github.com/PowerShellMafia/PowerSploit
    https://github.com/outflanknl/Recon-AD
)
add_submodules_to_dir Windows/AD "${windows_AD[@]}"

windows_kerberos=(
    https://github.com/ropnop/kerbrute
    https://github.com/nidem/kerberoast
    https://github.com/GhostPack/Rubeus
    https://github.com/cyberark/RiskySPN
    https://github.com/TarlogicSecurity/tickey
    https://github.com/Zer1t0/ticket_converter
    https://github.com/TarlogicSecurity/kerbrute
)
add_submodules_to_dir Windows/Kerberos "${windows_kerberos[@]}"

windows_loaders=(
    https://github.com/rasta-mouse/TikiTorch
    https://github.com/dtrizna/DotNetInject
    https://github.com/bohops/GhostBuild
    https://github.com/fullmetalcache/PowerLine
    https://github.com/Mr-Un1k0d3r/DLLsForHackers
    https://github.com/NullArray/MaliciousDLLGen
    https://github.com/trustedsec/unicorn
    https://github.com/mdsecactivebreach/SharpShooter
)
add_submodules_to_dir Windows/Loaders "${windows_loaders[@]}"


# linux
linux_enumerators=(
    https://github.com/rebootuser/LinEnum
    https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite
    https://github.com/Anon-Exploiter/SUID3NUM
    https://github.com/sleventyeleven/linuxprivchecker
    https://github.com/diego-treitos/linux-smart-enumeration
    https://github.com/pentestmonkey/unix-privesc-check
)
add_submodules_to_dir Linux/Enumerators "${linux_enumerators[@]}"

linux_backdoors=(
    https://github.com/blendin/3snake
)
add_submodules_to_dir Linux/Backdoors "${linux_backdoors[@]}"

linux_misc=(
    https://github.com/jfredrickson/setuid-wrapper
    https://github.com/DominicBreuker/pspy
    https://github.com/s0md3v/Decodify
    https://github.com/Bashfuscator/Bashfuscator
    https://github.com/skelsec/pypykatz
    https://github.com/andrew-d/static-binaries
    https://github.com/static-linux/static-binaries-i386
    https://github.com/ZephrFish/static-tools
    https://github.com/therealsaumil/static-arm-bins
)
add_submodules_to_dir Linux/Miscellaneous "${linux_misc[@]}"

# network
network_exploitation=(
    https://github.com/bettercap/bettercap
    https://github.com/byt3bl33d3r/CrackMapExec
    https://github.com/ollypwn/SMBGhost
    https://github.com/lgandx/Responder
    https://github.com/fox-it/mitm6
    https://github.com/threat9/routersploit
    https://github.com/DanMcInerney/net-creds
    https://github.com/RUB-NDS/PRET
    https://github.com/SecureAuthCorp/impacket
    https://github.com/s0lst1c3/eaphammer
    https://github.com/wifiphisher/wifiphisher
)
add_submodules_to_dir Network/Exploitation "${network_exploitation[@]}"

network_scanning=(
    https://github.com/robertdavidgraham/masscan
    https://github.com/rvrsh3ll/eavesarp
    https://github.com/BloodHoundAD/BloodHound
    https://github.com/fox-it/BloodHound.py
    https://github.com/ShawnDEvans/smbmap
    https://github.com/vulnersCom/nmap-vulners
    https://github.com/pr4jwal/CVE-2020-0796
)
add_submodules_to_dir Network/Scanning "${network_scanning[@]}"

network_pivoting=(
    https://github.com/kost/revsocks
    https://github.com/jpillora/chisel
    https://github.com/klsecservices/rpivot
    https://github.com/nccgroup/vlan-hopping---frogger
    https://github.com/cytopia/pwncat
    https://github.com/lesnuages/hershell
    https://github.com/Hackplayers/evil-winrm
)
add_submodules_to_dir Network/Pivoting "${network_pivoting[@]}"

misc_tools=(
    https://github.com/paranoidninja/CarbonCopy
    https://github.com/projectdiscovery/notify
    https://github.com/HarmJ0y/CheatSheets
    https://github.com/ZerBea/hcxtools
    https://github.com/ZerBea/hcxdumptool
    https://github.com/hashcat/hashcat-utils
    https://github.com/sdcampbell/Internal-Pentest-Playbook
    https://github.com/kmkz/Pentesting
    https://github.com/megadose/holehe
    https://github.com/nil0x42/duplicut
    https://github.com/jakejarvis/awesome-shodan-queries
    https://github.com/shifa123/shodandorks/blob/master/shodandorks
    https://github.com/codebox/homoglyph
    https://github.com/blackberry/pe_tree
    https://github.com/Ciphey/Ciphey
    https://github.com/vysecurity/LinkedInt
    https://github.com/S1ckB0y1337/Active-Directory-Exploitation-Cheat-Sheet
    https://github.com/leostat/rtfm
)
add_submodules_to_dir Miscellaneous "${misc_tools[@]}"

function download_github_tool_release() {
    wget -q --show-progress --no-check-certificate -nc https://jitpack.io/com/github/frohoff/ysoserial/master-SNAPSHOT/ysoserial-master-SNAPSHOT.jar -P ./Bin/Linux
    wget -q --show-progress --no-check-certificate -nc https://github.com/BloodHoundAD/BloodHound/releases/download/4.0.2/BloodHound-linux-x64.zip -P ./Bin/Linux
    wget -q --show-progress --no-check-certificate -nc https://github.com/michenriksen/aquatone/releases/download/v1.7.0/aquatone_linux_amd64_1.7.0.zip -P ./Bin/Linux

    wget -q --show-progress --no-check-certificate -nc https://github.com/pwntester/ysoserial.net/releases/download/v1.34/ysoserial-1.34.zip -P ./Bin/Windows
    wget -q --show-progress --no-check-certificate -nc https://github.com/michenriksen/aquatone/releases/download/v1.7.0/aquatone_windows_amd64_1.7.0.zip -P ./Bin/Windows
    wget -q --show-progress --no-check-certificate -nc https://github.com/BloodHoundAD/BloodHound/releases/download/4.0.2/BloodHound-win32-x64.zip -P ./Bin/Windows

    wget -q --show-progress --no-check-certificate -nc https://github.com/hashcat/hashcat/releases/download/v6.1.1/hashcat-6.1.1.7z -P ./Bin
    wget -q --show-progress --no-check-certificate -nc https://github.com/gentilkiwi/mimikatz/releases/download/2.2.0-20200918-fix/mimikatz_trunk.7z -P ./Bin/Windows

    wget -q --show-progress --no-check-certificate -nc https://raw.githubusercontent.com/iBotPeaches/Apktool/master/scripts/linux/apktool -P ./Bin/Mobile
    wget -q --show-progress --no-check-certificate -nc https://bitbucket.org/iBotPeaches/apktool/downloads/apktool_2.4.1.jar -P ./Bin/Mobile
    wget -q --show-progress --no-check-certificate -nc https://github.com/pxb1988/dex2jar/releases/download/2.0/dex-tools-2.0.zip -P ./Bin/Mobile
    wget -q --show-progress --no-check-certificate -nc https://github.com/appium-boneyard/sign/releases/download/1.0/sign-1.0.jar -P ./Bin/Mobile
    wget -q --show-progress --no-check-certificate -nc https://github.com/appium-boneyard/sign/releases/download/1.0/signapk-1.0.jar -P ./Bin/Mobile
    wget -q --show-progress --no-check-certificate -nc https://github.com/java-decompiler/jd-gui/releases/download/v1.6.6/jd-gui-1.6.6-min.jar -P ./Bin/Mobile

    #wget -q --show-progress --no-check-certificate -nc -r --no-parent https://live.sysinternals.com -P ./bin/windows
}
download_github_tool_release
